#!/usr/bin/env bash

cd build

cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Release
if [ $? -ne 0 ]
then
  echo "Failure: Refresh cmake failed" >&2
  exit 1
fi

cmake --build .
if [ $? -ne 0 ]
then
  echo "Failure: build failed" >&2
  exit 1
fi

cd ..