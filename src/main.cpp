
/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../configuration/config.hpp"
#include "Core/ConfigLoader.hpp"
#include <Ledger/Generator.hpp>
#include <Ledger/Wrapper.hpp>
#include <Ofx/OfxParser.hpp>
#include <Qif/QifParser.hpp>

#include <boost/program_options.hpp>
#include <filesystem>
#include <iostream>
#include <optional>
#include <spdlog/spdlog.h>
#include <string>
namespace fs = std::filesystem;

namespace po = boost::program_options;

struct ArgParsingResult {
        std::optional<int> exit_code;
        std::string filename;
        std::string journal_filename;
};
ArgParsingResult parse_args(int argc, char* argv[]);
std::vector<Transaction> load_file_transactions(const std::string& filename);
std::vector<Transaction> remove_old_transactions(const std::vector<Transaction>& transactions, const std::string& journal_filename);
void process(const std::string& filename, const std::string& journal_filename);
ConfigLoader get_config();
void render_transactions(const std::vector<Transaction>& transactions);

int main(int argc, char* argv[])
{
        const auto args_provided = parse_args(argc, argv);
        if (args_provided.exit_code) {
                return args_provided.exit_code.value();
        }

        process(args_provided.filename, args_provided.journal_filename);

        return EXIT_SUCCESS;
}

ArgParsingResult parse_args(int argc, char* argv[])
{
        po::options_description desc("Allowed options");
        desc.add_options()(
                "help,h", "Produce help message")(
                "file,f", po::value<std::string>(), "Set the input file. Token is optional and filename can be set directly.")("journal,j", po::value<std::string>(), "Set ledger journal file")("debug,d", "Set debugging mode");

        po::positional_options_description p;
        p.add("file", -1);

        po::command_line_parser po_parser{ argc, argv };
        po_parser.options(desc).positional(p);
        po::parsed_options parsed_options = po_parser.run();

        po::variables_map vm;
        po::store(parsed_options, vm);
        po::notify(vm);

        // Args processing.
        if (vm.count("debug")) {
                spdlog::set_level(spdlog::level::debug);
        }
        if (vm.count("help")) {
                fmt::print("Ledger-Sync {}, the OFX/QIF file into Ledger CLI transactions tool\n\n", bin_version);
                fmt::print("Copyright (C) 2019-2021 Geoffrey SURELLE.\n\n");
                std::cout << desc << std::endl;
                return { EXIT_SUCCESS };
        }
        if (!vm.count("file")) {
                spdlog::critical("No input file provided");
                return { EXIT_FAILURE };
        }

        ArgParsingResult result;
        result.filename = vm["file"].as<std::string>();
        if (vm.count("journal")) {
                result.journal_filename = vm["journal"].as<std::string>();
        }
        return result;
}

std::vector<Transaction> load_file_transactions(const std::string& filename)
{
        std::unique_ptr<FileParser> parser;

        const std::string filetype = fs::path(filename).extension().string();

        if (filetype == ".ofx") {
                // Load Ofx file.
                parser = std::make_unique<Ofx::OfxParser>();
        } else if (filetype == ".qif") {
                // Load Qif file.
                parser = std::make_unique<QifParser>();
        } else {
                spdlog::error("Filetype {} is not implemented.", filetype);
                return {};
        }

        parser->parse(filename);
        return std::move(parser->get_transactions());
}

std::vector<Transaction> remove_old_transactions(const std::vector<Transaction>& transactions, const std::string& journal_filename)
{
        std::vector<Transaction> result;

        Ledger::Wrapper ledger(journal_filename);
        std::copy_if(
                transactions.begin(),
                transactions.end(),
                std::back_inserter(result),
                [&](const auto& t_trans) {
                        return !ledger.is_transaction_exists(t_trans.id);
                });

        return std::move(result);
}

void process(const std::string& filename, const std::string& journal_filename)
{
        auto input_file_transactions = load_file_transactions(filename);
        std::sort(
                input_file_transactions.begin(),
                input_file_transactions.end(),
                [](const Transaction& a, const Transaction& b) {
                        return a.date < b.date;
                });
        const auto transactions_to_render = remove_old_transactions(input_file_transactions, journal_filename);
        render_transactions(transactions_to_render);
}

void render_transactions(const std::vector<Transaction>& transactions)
{
        const auto& config_loader = get_config();

        spdlog::debug("");
        spdlog::debug("Output transactions generation begin");
        Ledger::Generator generator(&config_loader.get());
        for (const auto& trans : transactions)
                std::cout << generator.generate(trans);
        spdlog::debug("Output transactions generation end");
}

ConfigLoader get_config()
{
        ConfigLoader config_loader;
        config_loader.load(".ledger-sync");

        return std::move(config_loader);
}