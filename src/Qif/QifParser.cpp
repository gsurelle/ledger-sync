/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "QifParser.hpp"

#include <Core/FileType.hpp>
#include <fstream>
#include <spdlog/spdlog.h>
#include <utils/sha.hpp>
#include <utils/string.hpp>

QifParser::QifParser()
        : FileParser()
{
}

void QifParser::parse(const std::string& filename)
{
        _transactions.clear();

        // input file.
        std::ifstream infile(filename);
        std::string line;

        Transaction temp_transaction("0");
        while (std::getline(infile, line)) {
                char line_type = line[0];
                switch (line_type) {
                case '!':
                        break;

                case 'D': {
                        auto date_partition = Utils::partition(line.substr(1), '/');
                        int date = 0;
                        date += std::stoi(date_partition[2]) * 10000;
                        date += std::stoi(date_partition[1]) * 100;
                        date += std::stoi(date_partition[0]);
                        temp_transaction.date = date;
                        break;
                }

                case 'T': {
                        if (line[1] == '-') {
                                const auto partition = Utils::partition(line.substr(2), '.');
                                temp_transaction.amount = Amount(std::stoi(partition[0]) * 100 + std::stoi(partition[1]), "EUR");
                                temp_transaction.amount = Amount::invert_amount(temp_transaction.amount);
                        } else {
                                const auto partition = Utils::partition(line.substr(1), '.');
                                temp_transaction.amount = Amount(std::stoi(partition[0]) * 100 + std::stoi(partition[1]), "EUR");
                        }
                        break;
                }

                case 'P':
                        temp_transaction.payee = line.substr(1);
                        Utils::trim(temp_transaction.payee);
                        break;

                case '^': {
                        // Generate the id
                        const std::string id_message = fmt::format(
                                "{}{}{}",
                                temp_transaction.date,
                                temp_transaction.payee,
                                temp_transaction.amount.get_value());
                        temp_transaction.id = fmt::format("{}.{}", (int)FileType::QIF, Utils::sha1(id_message));

                        _transactions.push_back(temp_transaction);
                        temp_transaction = Transaction("0");
                        break;
                }

                default:
                        break;
                }
        }
}
