/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <libxml/tree.h>
#include <string>
#include <vector>

namespace Ledger {
class Wrapper {
    public:
        Wrapper(const std::string& journal_file);
        ~Wrapper();

        bool is_transaction_exists(const std::string& id) const;

    private:
        std::string _journal_filename;

        void load_journal_ids();
        std::vector<std::string> _journal_ids;

        void load_transactions(const std::string& journal_xml);
        void load_transaction(xmlNode* transaction_node);

        std::string get_ledger_xml_extract();
};
} // namespace Ledger