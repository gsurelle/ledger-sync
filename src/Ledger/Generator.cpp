/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Generator.hpp"

#include <fmt/format.h>
#include <sstream>
#include <utils/Date.hpp>
#include <utils/string.hpp>

namespace Ledger {
Generator::Generator(const Config* config)
        : _config(config)
{
}

Generator::~Generator()
{
}

std::string Generator::generate(const Transaction& tran) const
{
        std::stringstream result;

        // Headers lines.
        result << generate_header(tran.date, tran.payee);
        result << generate_id(tran.id);

        const auto& account = _config->accounts.find(tran.account);
        result << generate_io(
                account != _config->accounts.end() ? account->second : _config->default_input_account,
                tran.amount);

        const LedgerRule* rule = search_rule(_config, tran.payee);
        result << generate_io(
                rule ? rule->account : _config->default_output_account,
                Amount::invert_amount(tran.amount));

        result << "\n";
        return result.str();
}

std::string Generator::generate_header(const int date, const std::string& payee)
{
        return std::move(fmt::format("{} {}\n", Utils::convert_int_to_strdate(date), payee));
}

std::string Generator::generate_id(const std::string& id) const
{
        // Transaction identifier is output as a meta tag.
        return std::move(fmt::format("{}; t_id: {}\n", Utils::generate_spaces(_config->tab_size), id));
}

std::string Generator::generate_io(const std::string& account, const Amount& amount) const
{
        // Amount to string.
        const std::string amount_str = fmt::format(
                "{}{}.{:02d} {}",
                amount.get_value() < 0 ? "-" : "",
                std::abs(amount.get_value()) / 100,
                std::abs(amount.get_value()) % 100,
                amount.get_currency());

        // Output.
        return std::move(fmt::format("{}{}{}{}\n",
                Utils::generate_spaces(_config->tab_size),
                account,
                Utils::generate_spaces(_config->line_size
                        - _config->tab_size
                        - Utils::to_wstring(account).length() // conversion into wstring for accentuated characters.
                        - amount_str.size()),
                amount_str));
}

} // namespace Ledger