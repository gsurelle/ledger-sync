/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Wrapper.hpp"

#include <fmt/format.h>
#include <libxml/parser.h>
#include <spdlog/spdlog.h>
#include <utils/Shell.hpp>
#include <utils/xml.hpp>

const std::string LEDGER_NO_JOURNAL_FOUND_ERROR = "Error: No journal file was specified (please use -f)";
const std::string LEDGER_SYNC_NO_JOURNAL_FOUND_ERROR = "Error: No journal file was found by ledger (please use -j)";

namespace Ledger {

Wrapper::Wrapper(const std::string& journal_file)
        : _journal_filename(journal_file)
{
        load_journal_ids();
}

void Wrapper::load_transactions(const std::string& journal_xml)
{
        xmlDocPtr doc;
        doc = xmlParseDoc(to_xml_charstring(journal_xml));

        xmlNode* root_element = xmlDocGetRootElement(doc);
        if (!root_element)
                return;

        auto transactions_nodes = find_children_nodes(root_element, "transactions");
        assert(transactions_nodes.size() == 1);
        auto transactions_node = transactions_nodes.front();

        auto transaction_nodes = find_children_nodes(transactions_node, "transaction");
        spdlog::debug("Number of transactions fetched from Ledger: {}", transaction_nodes.size());

        for (auto& transaction_node : transaction_nodes) {
                load_transaction(transaction_node);
        }

        xmlFreeDoc(doc);
        xmlCleanupParser();
}

void Wrapper::load_transaction(xmlNode* transaction_node)
{
        /**
         * Example
        <transactions>
                <transaction>
                        <date>2019/10/04</date>
                        <payee>MASTERCARD FEES            03/10</payee>
                        <note> t_id: 1.45678542163.5654070307777</note>
                        <metadata>
                                <value key="t_id">
                                        <string>1.45678542163.5654070307777</string>
                                </value>
                        </metadata>
                        ...
                </transaction>
        </transactions>
         */
        auto metadata_node = find_children_nodes(transaction_node, "metadata");
        assert(metadata_node.size() <= 1);

        if (metadata_node.size()) {
                auto value_nodes = find_children_nodes(metadata_node.front(), "value");

                for (auto& value_node : value_nodes) {
                        auto key_prop_value = xmlGetProp(value_node, to_xml_charstring("key"));

                        if (key_prop_value) {
                                if (to_stdstring(key_prop_value) == "t_id") {
                                        auto string_nodes = find_children_nodes(value_node, "string");
                                        assert(string_nodes.size() == 1);
                                        auto string_node = string_nodes.front();

                                        const std::string transaction_id = std::move(to_stdstring(xmlNodeGetContent(string_node)));
                                        spdlog::debug("Transaction {} loaded", transaction_id);
                                        _journal_ids.push_back(transaction_id);
                                }
                        }
                }
        }
}

std::string Wrapper::get_ledger_xml_extract() 
{
        const std::string all_trans_xml_query = fmt::format("ledger xml{} 2>&1", _journal_filename.size() ? fmt::format(" -f \"{}\"", _journal_filename) : "");
        spdlog::debug("Query all Ledger transactions in xml query: {}", all_trans_xml_query);

        const std::string extract_result = (Utils::exec_command(all_trans_xml_query));

        if (extract_result.find(LEDGER_NO_JOURNAL_FOUND_ERROR) != std::string::npos) {
                spdlog::critical(LEDGER_SYNC_NO_JOURNAL_FOUND_ERROR);
                return {};
        }
        
        return extract_result;
}

Wrapper::~Wrapper()
{
}

bool Wrapper::is_transaction_exists(const std::string& t_id) const
{
        return std::find(_journal_ids.begin(), _journal_ids.end(), t_id) != _journal_ids.end();
}

void Wrapper::load_journal_ids() 
{
        spdlog::debug("");
        spdlog::debug("Ledger transactions fetch begin");

        const std::string extract_result = get_ledger_xml_extract();

        load_transactions(extract_result);

        spdlog::debug("Ledger transactions fetch end");
}
} // namespace Ledger