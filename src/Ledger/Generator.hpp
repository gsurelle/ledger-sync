/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <Core/Config.hpp>
#include <Core/Transaction.hpp>
#include <string>

namespace Ledger {
class Generator {
    public:
        Generator(const Config* config);
        ~Generator();

        std::string generate(const Transaction& transaction) const;

    private:
        const Config* _config;

        static std::string generate_header(const int date, const std::string& payee);
        std::string generate_id(const std::string& id) const;
        std::string generate_io(const std::string& account, const Amount& amount) const;
};
} // namespace Ledger