/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Amount.hpp"

#include <cassert>

Amount::Amount()
        : _value(0)
{
        check_validity();
}
Amount::Amount(int value, const std::string& currency)
        : _value(value)
        , _currency(currency)
{
        check_validity();
}

int Amount::get_value() const
{
        assert(_is_valid);
        return _value;
}
 
const std::string& Amount::get_currency() const
{
        assert(_is_valid);
        return _currency;
}

void Amount::set_value(int value)
{
        _value = value;
        check_validity();
}

void Amount::set_currency(const std::string& currency)
{
        _currency = currency;
        check_validity();
}

Amount Amount::invert_amount(const Amount& input)
{
        return Amount(input.get_value() * (-1), input.get_currency());
}

void Amount::check_validity()
{
        _is_valid = !_currency.empty();
}
