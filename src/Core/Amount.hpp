/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <fmt/format.h>
#include <string>

class Amount {
    public:
        Amount();
        Amount(int value, const std::string& currency);

        int get_value() const;
        const std::string& get_currency() const;

        void set_value(int value);
        void set_currency(const std::string& currency);

        static Amount invert_amount(const Amount& input);

    private:
        bool _is_valid;
        void check_validity();

        int _value;
        std::string _currency;
};

inline bool operator==(const Amount& lhs, const Amount& rhs)
{
        return lhs.get_value() == rhs.get_value() && lhs.get_currency() == lhs.get_currency();
}

template <>
struct fmt::formatter<Amount> {
        constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const Amount& d, FormatContext& ctx)
        {
                return format_to(ctx.out(), "[Amount {}cts {}]", d.get_value(), d.get_currency());
        }
};
