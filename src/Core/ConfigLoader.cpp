/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConfigLoader.hpp"
#include <fstream>
#include <spdlog/spdlog.h>
#include <toml/toml.h>

const Config DEFAULT_CONFIG = {
        4, 65, "Expenses:Misc", "Assets:Misc"
};

ConfigLoader::ConfigLoader(/* args */)
        : _config(DEFAULT_CONFIG)
{
}

ConfigLoader::~ConfigLoader()
{
}

void ConfigLoader::load(const std::string& filename)
{
        std::ifstream ifs(filename);
        toml::ParseResult pr = toml::parse(ifs);

        if (!pr.valid()) {
                spdlog::warn(".ledger-sync does not exists");
                return;
        }

        const auto& v = pr.value;

#define GETPARAM(node, type, dest)                                                            \
        if (const auto p = v.find(node)) {                                                    \
                if (!p->is<type>()) {                                                         \
                        spdlog::critical("Parameter {} is not in the {} type.", node, #type); \
                        throw std::runtime_error("");                                         \
                }                                                                             \
                dest = p->as<type>();                                                         \
        }

        GETPARAM("template.tabSize", int, _config.tab_size)
        GETPARAM("template.lineSize", int, _config.line_size)
        GETPARAM("default.input", std::string, _config.default_input_account)
        GETPARAM("default.output", std::string, _config.default_output_account)

        if (const auto& accs = v.find("accounts")) {
                if (accs->is<toml::Table>()) {
                        for (const auto& acc : accs->as<toml::Table>()) {
                                const std::string account = acc.first;
                                const std::string input = acc.second.as<std::string>();

                                _config.accounts.emplace(account, input);
                        }
                }
        }
        const auto& arp = v.find("rule");
        const auto& ar = arp->as<toml::Array>();
        for (const auto& ar_itm : ar) {
                LedgerRule rule;

                rule.account = _config.default_output_account;
                if (const auto& account = ar_itm.find("account")) {
                        if (account->is<std::string>()) {
                                rule.account = account->as<std::string>();
                        }
                }
                if (const auto& have = ar_itm.find("have")) {
                        if (have->is<std::string>()) {
                                rule.have.push_back(have->as<std::string>());
                        } else if (have->is<toml::Array>()) {
                                for (const auto& input_itm : have->as<toml::Array>()) {
                                        assert(input_itm.is<std::string>());
                                        rule.have.push_back(input_itm.as<std::string>());
                                }
                        }
                }
                if (const auto& have_not = ar_itm.find("have_not")) {
                        if (have_not->is<std::string>()) {
                                rule.have_not.push_back(have_not->as<std::string>());
                        } else if (have_not->is<toml::Array>()) {
                                for (const auto& input_itm : have_not->as<toml::Array>()) {
                                        assert(input_itm.is<std::string>());
                                        rule.have_not.push_back(input_itm.as<std::string>());
                                }
                        }
                }

                _config.rules.push_back(std::move(rule));
        }
}
