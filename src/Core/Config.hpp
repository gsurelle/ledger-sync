/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Ledger/LedgerRule.hpp"
#include <string>
#include <vector>
#include <map>

struct Config {
        int tab_size;
        int line_size;

        std::string default_output_account;
        std::string default_input_account;

        std::map<std::string, std::string> accounts;

        std::vector<LedgerRule> rules;
};

inline const LedgerRule* search_rule(const Config* config, const std::string& payee)
{
        bool found = false;
        for (auto& rule : config->rules) {
                found = false;

                for (auto& have : rule.have) {
                        if (payee.find(have) != std::string::npos)
                                found = true;
                        else
                                found = false;
                }

                for (auto& have_not : rule.have_not) {
                        if (payee.find(have_not) != std::string::npos)
                                found = false;
                }

                if (found)
                        return &rule;
        }

        return nullptr;
}
