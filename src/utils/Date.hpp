/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <fmt/format.h>
#include <string>

namespace Utils {
/**
 * @brief Convert a date contain in a int, into a std::string.
 * 
 * @param date The date in a int type. Example : int 20200412 => std::string 2020/04/2020.
 * @return std::string The date in the std::string format.
 */
inline std::string convert_int_to_strdate(int date)
{
        int year = date / 10000;
        int month = (date / 100) % 100;
        int day = date % 100;

        return fmt::format("{:04d}/{:02d}/{:02d}", year, month, day);
}
} // namespace Utils