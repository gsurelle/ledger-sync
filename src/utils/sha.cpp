/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sha.hpp"
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>

namespace Utils {

std::string sha1(const std::string& t_message)
{
        CryptoPP::SHA1 hash;
        CryptoPP::byte digest[CryptoPP::SHA1::DIGESTSIZE];

        hash.CalculateDigest(digest, (CryptoPP::byte*)t_message.c_str(), t_message.length());

        CryptoPP::HexEncoder encoder;
        std::string output;
        encoder.Attach(new CryptoPP::StringSink(output));
        encoder.Put(digest, sizeof(digest));
        encoder.MessageEnd();

        return output;
}

} // namespace Utils
