/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cctype>
#include <codecvt>
#include <locale>
#include <sstream>
#include <string>

namespace Utils {
/**
 * @brief Convert a std::string to a std::wstring.
 * 
 * @param str std::string to convert. 
 * @return std::wstring converted.
 */
static inline std::wstring to_wstring(const std::string& str)
{
        static std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;

        return converter.from_bytes(str);
}

/**
 * @brief Create a std::string that contain n spaces.
 * 
 * @param count The number of spaces.
 * @return std::string The generated std::string.
 */
static inline std::string generate_spaces(const std::size_t count)
{
        return std::move(std::string(count, ' '));
}

// trim from start (in place)
static inline void ltrim(std::string& s)
{
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
                return !std::isspace(ch);
        }));
}

// trim from end (in place)
static inline void rtrim(std::string& s)
{
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
                return !std::isspace(ch);
        }).base(),
                s.end());
}

// trim from both ends (in place)
static inline void trim(std::string& s)
{
        ltrim(s);
        rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s)
{
        ltrim(s);
        return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s)
{
        rtrim(s);
        return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s)
{
        trim(s);
        return s;
}

inline std::vector<std::string> partition(const std::string& str, const char token)
{
        std::vector<std::string> strings;
        std::istringstream stream(str);
        std::string temp_str;
        while (std::getline(stream, temp_str, token)) {
                strings.push_back(temp_str);
        }
        return std::move(strings);
}

inline std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case std::streambuf::traits_type::eof():
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

} // namespace Utils