/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <libxml/tree.h>
#include <string>
#include <vector>

inline std::string to_stdstring(const xmlChar* input)
{
        const char* char_str = (const char*)input;
        return std::move(std::string(char_str));
}

inline const xmlChar* to_xml_charstring(const std::string& input)
{
        const xmlChar* result_xml_char = (const xmlChar*)input.c_str();
        return result_xml_char;
}

inline std::vector<xmlNode*> get_children_nodes(xmlNode* parent)
{
        xmlNode* cur_node = nullptr;

        std::vector<xmlNode*> result;
        for (cur_node = parent->children; cur_node; cur_node = cur_node->next) {
                if (cur_node->type == XML_ELEMENT_NODE) {
                        result.push_back(cur_node);
                }
        }
        return result;
}

inline std::vector<xmlNode*> find_children_nodes(xmlNode* parent, const std::string& key)
{
        std::vector<xmlNode*> result;
        for (auto& child : get_children_nodes(parent)) {
                if (to_stdstring(child->name) == key) {
                        result.push_back(child);
                }
        }

        return result;
}