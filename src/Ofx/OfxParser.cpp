/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OfxParser.hpp"
#include "OfxParser102.hpp"
#include "OfxParser200.hpp"
#include <fstream>
#include <spdlog/spdlog.h>
#include <string>

namespace Ofx {

OfxParser::OfxParser()
        : FileParser()
{
}

void OfxParser::parse(const std::string& t_filename)
{
        enum class OfxVersion {
                OFX_102,
                OFX_200,
        };

        OfxVersion version;
        {
                std::ifstream infile(t_filename);
                std::string line;
                std::getline(infile, line);
                if (line[0] == '<') {
                        version = OfxVersion::OFX_200;
                } else {
                        version = OfxVersion::OFX_102;
                }
        }

        switch (version) {
        case OfxVersion::OFX_102:
                spdlog::debug("Ofx version 102");
                _parser = std::make_unique<OfxParser102>();
                break;

        case OfxVersion::OFX_200:
                spdlog::debug("Ofx version 200");
                _parser = std::make_unique<OfxParser200>();
                break;

        default:
                spdlog::critical("Ofx version not implemented");
                return;
        }

        _parser->parse(t_filename);
}

static const std::vector<Transaction> EMPTY_VECTOR;
const std::vector<Transaction>& OfxParser::get_transactions() const
{
        if (_parser)
                return _parser->get_transactions();
        else
                return EMPTY_VECTOR;
}

} // namespace Ofx