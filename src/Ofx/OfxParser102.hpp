/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <Core/FileParser.hpp>
#include <map>

namespace Ofx {

class OfxParser102 : public FileParser {
    public:
        OfxParser102();
        ~OfxParser102() = default;

        void parse(const std::string& filename) override;

    private:
        std::map<std::string, std::string> _headers;

        static std::string generate_payee(const std::string& id, const std::string& name, const std::string& memo);

        static const bool is_header_line(const std::string& line);
};

} // namespace Ofx