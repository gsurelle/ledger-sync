/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "OfxParser102.hpp"

#include "SGML/SGMLNode.hpp"

#include <Core/FileType.hpp>
#include <algorithm>
#include <cassert>
#include <fstream>
#include <spdlog/spdlog.h>
#include <string>
#include <utils/string.hpp>
#include <vector>

namespace Ofx {

OfxParser102::OfxParser102()
        : FileParser()
{
}

void OfxParser102::parse(const std::string& t_filename)
{
        _transactions.clear();

        // input file.
        std::ifstream infile(t_filename);

        enum State {
                HEADERS,
                CONTENT
        };
        State state = HEADERS;

        std::string line;
        std::vector<std::string> content_lines;
        while (Utils::safeGetline(infile, line)) {
                if (!line.empty()) {
                        if (state == HEADERS) {
                                if (is_header_line(line)) {
                                        const std::size_t separator = line.find_first_of(":");
                                        _headers.emplace(line.substr(0, separator), line.substr(separator + 1));
                                } else {
                                        state = CONTENT;
                                }
                        }
                        if (state == CONTENT) {
                                content_lines.push_back(line);
                        }
                }
        }

        if (content_lines.empty()) {
                spdlog::critical("Input file is empty");
                return;
        }

        const SGMLNode sgml_content(content_lines);

        const auto BANKMSGSRSV1 = sgml_content.find_child("BANKMSGSRSV1");
        if (!BANKMSGSRSV1) {
                spdlog::critical("OFX file corrupted (no BANKMSGSRSV1)");
                _headers.clear();
                _transactions.clear();
                return;
        }

        // Only one bank msg is excepted, but in case of...
        for (const auto& child : BANKMSGSRSV1->get_children()) {
                if (child->get_title() == "STMTTRNRS") {
                        const auto& STMTTRNRS = child;

                        const auto STMTRS = STMTTRNRS->find_child("STMTRS");
                        if (!STMTRS) {
                                spdlog::critical("OFX file corrupted (no STMTRS)");
                                _headers.clear();
                                _transactions.clear();
                                return;
                        }

                        const std::string account_id = [&]() -> std::string {
                                const auto BANKACCTFROM = STMTRS->find_child("BANKACCTFROM");
                                if (!BANKACCTFROM) {
                                        spdlog::critical("OFX file corrupted (no BANKACCTFROM)");
                                        return std::string();
                                }
                                const auto ACCTID = BANKACCTFROM->get_attribute("ACCTID");
                                if (!ACCTID.has_value()) {
                                        spdlog::critical("OFX file corrupted (no ACCTID)");
                                        return std::string();
                                }
                                return *ACCTID.value();
                        }();
                        if (account_id.empty())
                                return;

                        const auto curdef_attr = STMTRS->get_attribute("CURDEF");
                        if (!curdef_attr.has_value()) {
                                spdlog::critical("Child CURDEF not found");
                                _headers.clear();
                                _transactions.clear();
                                return;
                        }
                        const std::string currency = *curdef_attr.value();

                        const auto BANKTRANLIST = STMTRS->find_child("BANKTRANLIST");
                        if (!BANKTRANLIST) {
                                spdlog::critical("OFX file corrupted (no BANKTRANLIST)");
                                _headers.clear();
                                _transactions.clear();
                                return;
                        }

                        for (const auto& transaction_read : BANKTRANLIST->get_children()) {
                                const auto fitid_attr = transaction_read->get_attribute("FITID");
                                if (!fitid_attr.has_value()) {
                                        spdlog::critical("Transaction doesn't have required FITID attribute");
                                        _headers.clear();
                                        _transactions.clear();
                                        return;
                                }
                                std::string id = fmt::format("{}.{}.{}",
                                        (int)FileType::OFX,
                                        account_id,
                                        *fitid_attr.value());
                                Transaction trans(id);
                                trans.account = account_id;

                                const auto dtposted_attr = transaction_read->get_attribute("DTPOSTED");
                                if (!dtposted_attr.has_value()) {
                                        spdlog::critical("Transaction {} doesn't have required DTPOSTED attribute", id);
                                        _headers.clear();
                                        _transactions.clear();
                                        return;
                                }
                                const std::string dt_posted = dtposted_attr.value()->substr(0, 8);
                                trans.date = std::stoi(dt_posted);

                                const auto trnamt_attr = transaction_read->get_attribute("TRNAMT");
                                if (!trnamt_attr.has_value()) {
                                        spdlog::critical("Transaction {} doesn't have required TRNAMT attribute", id);
                                        _headers.clear();
                                        _transactions.clear();
                                        return;
                                }
                                const int amount_value = SGMLNode::amount_to_cents(*trnamt_attr.value());
                                trans.amount = Amount(amount_value, currency);

                                const auto name_attr = transaction_read->get_attribute("NAME");
                                const auto memo_attr = transaction_read->get_attribute("MEMO");
                                const std::string name = name_attr.has_value() ? Utils::trim_copy(*name_attr.value()) : "";
                                const std::string memo = memo_attr.has_value() ? Utils::trim_copy(*memo_attr.value()) : "";
                                trans.payee = generate_payee(id, name, memo);

                                _transactions.push_back(std::move(trans));
                        }
                }
        }

        std::sort(_transactions.begin(), _transactions.end(), [](Transaction& a, Transaction& b) { return a.date < b.date; });
}

const bool OfxParser102::is_header_line(const std::string& line)
{
        if (line.front() == '<')
                return false;

        if (line.back() == '>')
                return false;

        if (line.find(":") == std::string::npos)
                return false;

        return true;
}

std::string OfxParser102::generate_payee(const std::string& id, const std::string& name, const std::string& memo)
{
        if (name.empty() && memo.empty())
                return id;

        if (memo.empty())
                return name;

        if (name.empty())
                return memo;

        return fmt::format("{} - {}", name, memo);
}
} // namespace Ofx