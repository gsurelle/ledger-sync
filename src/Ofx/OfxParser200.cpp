/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "OfxParser200.hpp"

#include <Core/FileType.hpp>
#include <fstream>
#include <libxml/parser.h>
#include <spdlog/spdlog.h>
#include <sstream>
#include <utils/string.hpp>
#include <utils/xml.hpp>

namespace Ofx {

OfxParser200::OfxParser200()
        : FileParser()
{
}

void OfxParser200::parse(const std::string& filename)
{
        std::ifstream infile(filename);
        std::stringstream file_stream;
        file_stream << infile.rdbuf();
        spdlog::debug(file_stream.str());

        xmlDocPtr doc;
        doc = xmlParseDoc(to_xml_charstring(file_stream.str()));

        xmlNode* root_element = xmlDocGetRootElement(doc);
        if (!root_element) {
                spdlog::error("OfxParser200::parse() no root element");
                return;
        }

        auto BANKMSGSRSV1_nodes = find_children_nodes(root_element, "BANKMSGSRSV1");
        if (!BANKMSGSRSV1_nodes.empty()) {

                auto STMTTRNRS_nodes = find_children_nodes(BANKMSGSRSV1_nodes[0], "STMTTRNRS");
                if (!STMTTRNRS_nodes.empty()) {

                        auto STMTRS_nodes = find_children_nodes(STMTTRNRS_nodes[0], "STMTRS");
                        if (!STMTRS_nodes.empty()) {

                                auto CURDEF = to_stdstring(xmlNodeGetContent(find_children_nodes(STMTRS_nodes[0], "CURDEF")[0]));

                                std::string BANKID, ACCTID;
                                auto BANKACCTFROM_nodes = find_children_nodes(STMTRS_nodes[0], "BANKACCTFROM");
                                if (!BANKACCTFROM_nodes.empty()) {
                                        //BANKID = toString(xmlNodeGetContent(find_children_nodes(BANKACCTFROM_nodes[0], "BANKID")[0]));
                                        ACCTID = to_stdstring(xmlNodeGetContent(find_children_nodes(BANKACCTFROM_nodes[0], "ACCTID")[0]));
                                }

                                auto BANKTRANLIST_nodes = find_children_nodes(STMTRS_nodes[0], "BANKTRANLIST");
                                if (!BANKTRANLIST_nodes.empty()) {

                                        auto STMTTRN_nodes = find_children_nodes(BANKTRANLIST_nodes[0], "STMTTRN");
                                        for (auto& STMTTRN_node : STMTTRN_nodes)
                                                load_transaction(STMTTRN_node, ACCTID, CURDEF);
                                }
                        }
                }
        }

        std::sort(
                _transactions.begin(),
                _transactions.end(),
                [](Transaction& a, Transaction& b) { return a.date < b.date; });
}

static const std::size_t DATETIME_SIZE = 14;
static const std::size_t DATE_SIZE = 8;
void OfxParser200::load_transaction(xmlNode* node, const std::string& account_id, const std::string& currency)
{
        std::string id = fmt::format("{}.{}.{}",
                (int)FileType::OFX,
                account_id,
                to_stdstring(xmlNodeGetContent(find_children_nodes(node, "FITID")[0])));

        Transaction trans(id);
        trans.account = account_id;

        auto date_str = to_stdstring(xmlNodeGetContent(find_children_nodes(node, "DTPOSTED")[0]));

        switch (date_str.size()) {
        case DATETIME_SIZE:
                trans.date = std::stoi(date_str.substr(0, 8));
                break;

        case DATE_SIZE:
                trans.date = std::stoi(date_str);
                break;

        default:
                break;
        };

        const int amount_value = parse_amount(to_stdstring(xmlNodeGetContent(find_children_nodes(node, "TRNAMT")[0])));
        spdlog::debug("amount = {}", amount_value);
        const std::string name = to_stdstring(xmlNodeGetContent(find_children_nodes(node, "NAME")[0]));
        const std::string memo = "";
        trans.payee = name;
        trans.amount = Amount(amount_value, currency);

        _transactions.push_back(std::move(trans));
}

int OfxParser200::parse_amount(const std::string& str)
{
        std::string ints, decimals;
        if (str.find('.') != std::string::npos) {
                const auto partition = Utils::partition(str, '.');
                ints = partition[0];
                decimals = partition[1];
        } else if (str.find(',') != std::string::npos) {
                const auto partition = Utils::partition(str, ',');
                ints = partition[0];
                decimals = partition[1];
        } else {
                ints = str;
                decimals = "00";
        }

        return ints[0] == '-'
                ? (std::stoi(ints) * 100) - std::stoi(decimals)
                : (std::stoi(ints) * 100) + std::stoi(decimals);
}

} // namespace Ofx