/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

class SGMLNode {
    public:
        SGMLNode(const std::vector<std::string>& lines);
        ~SGMLNode();

        static int amount_to_cents(const std::string& amount);

        const std::string& get_title() const { return _title; }

        const std::map<std::string, std::string>& get_attributes() const { return _attributes; }
        std::optional<const std::string*> get_attribute(const std::string& key) const ;

        const std::vector<std::unique_ptr<SGMLNode>>& get_children() const { return _children; }
        const SGMLNode* find_child(const std::string& title) const
        {
                for (auto& child : _children) {
                        if (child->get_title() == title)
                                return child.get();
                }
                return nullptr;
        }

    private:
        std::string _title;
        std::map<std::string, std::string> _attributes;
        std::vector<std::unique_ptr<SGMLNode>> _children;
};
