/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SGMLNode.hpp"

#include <iostream>
#include <sstream>

SGMLNode::SGMLNode(const std::vector<std::string>& lines)
{
        _title = lines[0].substr(1, lines[0].size() - 2);

        std::string current_node_key = "";
        std::vector<std::string> current_block_load;

        for (size_t i = 1; i < lines.size() - 1; i++) {
                const auto& line = lines.at(i);

                const std::size_t mark_end_pos = line.find_first_of(">");

                bool is_closure_mark = line[1] == '/';
                std::string mark;
                if (!is_closure_mark)
                        mark = line.substr(1, mark_end_pos - 1);
                else
                        mark = line.substr(2, mark_end_pos - 2);

                // Is it an attribute ?
                if (current_node_key.empty()) {
                        if (line.back() != '>')
                                _attributes.emplace(mark, line.substr(mark_end_pos + 1));
                        else
                                // Starting of the new block.
                                current_node_key = mark;
                }
                if (!current_node_key.empty()) {
                        // If currently in a block loading, we have to save the line.
                        current_block_load.push_back(line);

                        // Closure of the current block.
                        if (mark == current_node_key && is_closure_mark) {
                                _children.push_back(std::make_unique<SGMLNode>(current_block_load));

                                current_node_key.clear();
                                current_block_load.clear();
                        }
                }
        }
}

SGMLNode::~SGMLNode()
{
}

int SGMLNode::amount_to_cents(const std::string& amount)
{
        const bool is_negative = amount.front() == '-';

        std::size_t integer_pos_start = std::isdigit(amount[0]) ? 0 : 1;
        std::size_t integer_size = amount.size() - (3 + integer_pos_start);

        int result = 0;
        result += std::stoi(amount.substr(integer_pos_start, integer_size)) * 100;
        result += std::stoi(amount.substr(amount.size() - 2));

        if (is_negative)
                result *= -1;

        return result;
}

std::optional<const std::string*> SGMLNode::get_attribute(const std::string& key) const
{
        if(_attributes.count(key))
                return &_attributes.at(key);
        return std::nullopt;
}