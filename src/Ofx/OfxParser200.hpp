/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <Core/FileParser.hpp>
#include <libxml/tree.h>

namespace Ofx {

class OfxParser200 : public FileParser {
    public:
        OfxParser200();
        ~OfxParser200() = default;

        void parse(const std::string& filename) override;

    private:
        void load_transaction(xmlNode* node, const std::string& account_id, const std::string& currency);
        static int parse_amount(const std::string& str);
};

} // namespace Ofx