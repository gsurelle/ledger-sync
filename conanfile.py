from conan import ConanFile


class CompressorRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("fmt/9.1.0")
        self.requires("spdlog/1.11.0")
        self.requires("boost/1.81.0")
        self.requires("catch2/3.3.1")
        self.requires("libxml2/2.10.4")
        self.requires("cryptopp/8.7.0")

    def build_requirements(self):
        self.tool_requires("cmake/3.22.6")