/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Core/Config.hpp>
#include <Core/Transaction.hpp>
#include <Ledger/Generator.hpp>
#include <Ledger/Wrapper.hpp>
#include <catch2/catch_all.hpp>
#include <iostream>
#include <spdlog/spdlog.h>

TEST_CASE("Wrapper test")
{
        Ledger::Wrapper wrapper("test/data/journal1.ledger");

        const std::string id_exists = "1.45678542163.5654070307777";
        REQUIRE(wrapper.is_transaction_exists(id_exists));

        const std::string id_exists_not = "1.45678542163.6465498798798";
        REQUIRE_FALSE(wrapper.is_transaction_exists(id_exists_not));
}

TEST_CASE("Generator test")
{
        Config config{ 2, 60, "Expenses:Default", "Assets:Default", {},
                { { "Expenses:Alimentation:Carrefour", { "CARREFOUR_HAVE" }, {} } } };
        Ledger::Generator generator(&config);

        Transaction t1("1.tr.1");
        t1.date = 20201218;
        t1.amount = Amount(105, "USD");
        t1.payee = "IMPOTS TAXE D'HABITATION";

        std::string result = generator.generate(t1);
        std::string expected_result = "2020/12/18 IMPOTS TAXE D'HABITATION\n"
                                      "  ; t_id: 1.tr.1\n"
                                      "  Assets:Default                                    1.05 USD\n"
                                      "  Expenses:Default                                 -1.05 USD\n\n";
        //std::replace(result.begin(), result.end(), ' ', '_');
        //std::replace(expected_result.begin(), expected_result.end(), ' ', '_');
        REQUIRE(result == expected_result);

        Transaction t2("1.tr.2");
        t2.date = 20201213;
        t2.amount = Amount(-1, "MAD");
        t2.payee = "HABITATION CARREFOUR_HAVE";
        t2.account = "tr";

        config.accounts = { { "tr", "Assets:Check" } };
        result = generator.generate(t2);
        expected_result = "2020/12/13 HABITATION CARREFOUR_HAVE\n"
                          "  ; t_id: 1.tr.2\n"
                          "  Assets:Check                                     -0.01 MAD\n"
                          "  Expenses:Alimentation:Carrefour                   0.01 MAD\n\n";
        //std::replace(result.begin(), result.end(), ' ', '_');
        //std::replace(expected_result.begin(), expected_result.end(), ' ', '_');
        REQUIRE(result == expected_result);
}