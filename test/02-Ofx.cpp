/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <catch2/catch_all.hpp>

#include <Ofx/OfxParser.hpp>
#include <fmt/printf.h>

TEST_CASE("FileParser test")
{
        Ofx::OfxParser p;
        p.parse("test/data/extract_ofx_102.ofx");

        REQUIRE(p.get_transactions().size() == 4);

        const auto& t1 = p.get_transactions().at(0);
        const auto& t2 = p.get_transactions().at(1);
        const auto& t3 = p.get_transactions().at(2);
        const auto& t4 = p.get_transactions().at(3);

        REQUIRE(t1.account == "45678542163");
        REQUIRE(t2.account == "45678542163");

        REQUIRE(t1.amount == Amount(-2498, "EUR"));
        REQUIRE(t2.amount == Amount(197, "EUR"));
        REQUIRE(t3.amount == Amount(-5, "EUR"));
        REQUIRE(t4.amount == Amount(5, "EUR"));

        REQUIRE(t1.date == 20191004);
        REQUIRE(t2.date == 20191005);

        REQUIRE(t1.id == "1.45678542163.5654070307777");
        REQUIRE(t2.id == "1.45678542163.5651020319140");

        REQUIRE(t1.payee == "MASTERCARD FEES            03/10");
        REQUIRE(t2.payee == "GRAND FRAIS - MEMO_TEST");
}

TEST_CASE("FileParser test2")
{
        Ofx::OfxParser p;
        p.parse("test/data/extract_ofx_102_2.ofx");

        REQUIRE(p.get_transactions().size() == 1);

        const auto& t1 = p.get_transactions().at(0);

        REQUIRE(t1.account == "00004654879");

        REQUIRE(t1.amount == Amount(5000, "EUR"));

        REQUIRE(t1.date == 20231116);

        REQUIRE(t1.id == "1.00004654879.1234053205325914");

        REQUIRE(t1.payee == "VIR SEPA BKLA - OSEF");
}
