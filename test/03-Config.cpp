/*
    Copyright (C) 2019-2021  Geoffrey Surelle.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <catch2/catch_all.hpp>

#include <Core/ConfigLoader.hpp>

TEST_CASE("Config loader test")
{
        ConfigLoader loader;
        loader.load("test/data/config1.toml");
        const auto& config = loader.get();

        REQUIRE(config.tab_size == 4);
        REQUIRE(config.line_size == 65);
        REQUIRE(config.default_input_account == "Assets:Misc");
        REQUIRE(config.default_output_account == "Expenses:Misc");

        REQUIRE(config.accounts.size() == 1);
        REQUIRE(config.accounts.count("45678542163"));
        REQUIRE(config.accounts.at("45678542163") == "Assets:Check");

        REQUIRE(config.rules.size() == 2);
        REQUIRE(config.rules.at(0).account == "Expenses:Alimentation:Lidlà");
        REQUIRE(config.rules.at(0).have.size() == 1);
        REQUIRE(config.rules.at(0).have.at(0) == "LIDL");
        REQUIRE(config.rules.at(0).have_not.size() == 1);
        REQUIRE(config.rules.at(0).have_not.at(0) == "AUCHAN");

        REQUIRE(config.rules.at(1).account == "Expenses:Alimentation:Carrefour");
        REQUIRE(config.rules.at(1).have.size() == 2);
        REQUIRE(config.rules.at(1).have.at(0) == "COTIS COMPTE A COMPOSER");
        REQUIRE(config.rules.at(1).have.at(1) == "CARREFOUR");
        REQUIRE(config.rules.at(1).have_not.size() == 0);
}