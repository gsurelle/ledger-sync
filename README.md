# Ledger-Sync

Ledger-Sync is a CLI Ledger transactions generator. Ledger-Cli takes an OFX file and a config file as inputs, and generate the transaction with account selection.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

No prequisites needed.

### Installation

``` bash
git clone https://gitlab.com/gsurelle/ledger-sync.git
cd ledger-sync
mkdir build && cd build && conan install .. && cd ..
make
sudo make install
```

## Documentation

### Simple generation

``` bash
ledger-sync my_ofx_file.ofx
```

### Account selection

Ledger-Sync provide a simple system for automate the account selection for generated transactions. All of the configuration for the automatization is set in a [configuration file](#configuration-file). The user's ledger journal have to be specified for synchronization.

``` bash
ledger-sync my_ofx_file.ofx -j my_journal.ledger
```

For synchronize, ledger-sync request the specified journal with the meta data ofxid, set in each generated transaction. Here is a generated transaction example :

``` ledger-cli
2019/07/26 MAT MOLINEL SC             25/07
    ; ofxid: 1.12345678901.6546487985213
    Assets:Check                                        -7.10 EUR
    Expenses:Misc                                        7.10 EUR
```

### Configuration file

All Ledger-Sync parameters are set in a configuration file. That file is a TOML file, should be named .ledger-sync and should be in the working directory.
Here is an example :

```toml
[template]
tabSpace = 4
lineSize = 65

[default]
input = "Assets:Check"
output = "Expenses:Misc"
currency = "EUR"

[accounts]
12345678945 = "Assets:Cash"

[[rule]]
account = "Expenses:Alimentation:Lidl"
have = "LIDL"

[[rule]]
account = "Expenses:Alimentation:Carrefour"
have_not = ["COTIS COMPTE A COMPOSER", "CARREFOUR"]
```

You can see that we declared two rules. These rules defines the prerequisites to find which rule is to apply to a transaction (have and have_not members), and the output account to apply.

Notice that :

* Members have and have_not can be a simple string or a string array.
* All have and have_not requisites are in an big AND condition.
* You can define which accounts are applied when no rule is found for a transaction.

## License

This project is licensed under the GPLv3 License - see the LICENSE.txt file for details.
